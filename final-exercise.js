/*dictionary collection*/
var collection = [{"nome": "Pedro", "turma": "A", "nota1": 10, "nota2": 7}, {"nome": "Maria", "turma": "B", "nota1": 7, "nota2": 4}, {"nome": "Jonathan", "turma": "C", "nota1": 9, "nota2": 9}, {"nome": "Karina", "turma": "A", "nota1": 8, "nota2": 2}, {"nome": "Carlos", "turma": "B", "nota1": 10, "nota2": 10}, {"nome": "Cíntia", "turma": "C", "nota1": 6, "nota2": 5}] 

/*calculating grades*/
for(i in collection){
    elem = collection[i];
    media = (elem.nota1 + elem.nota2)/2;
    elem["media"] = media;
}

/*auxiliaries arrays*/
let medias = collection.map(aluno => aluno.media);
let turmas = [];
let melhores = [];

/*looking for highest grades*/
for(i in collection){
    elem = collection[i];
    if(i == 0){
        melhores.push(elem);
        turmas.push(elem.turma);
    }else{
        if((elem.media > medias[i-1]) && (turmas.indexOf(elem.turma) === -1)) {
            melhores.push(elem);
        }
    }
}

for(i in melhores){
    var output = 'O aluno '.concat(melhores[i].nome, ' teve a média mais alta da turma ', melhores[i].turma, ', com ',melhores[i].media);
    console.log(output);
}
